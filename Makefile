CC=gcc
CFLAGS=-I

all: main.c
	$(CC) -o test main.c

.PHONY: clean

clean:
	rm -f test